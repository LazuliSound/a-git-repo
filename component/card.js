const cardStyle = {
	margin: "1rem",
	flexBasis: "45%",
	padding: "1.5rem",
	textAlign: "left",
	color: "inherit",
	textDecoration: "none",
	border: "1px solid #eaeaea",
	borderRadius: "10px",
	transition: "color 0.15s ease, border-color 0.15s ease"
};

const h3Style = {
	margin: "0 0 1rem 0",
	fontSize: "1.5rem"
}

const pStyle = {
	margin: "0",
	fontSize: "1.25rem",
	lineHeight: "1.5"
}

export default function Card(props) {
  return (
			<a className="card" style={cardStyle}>
				<h3 style={h3Style}>{props.name}</h3>
				<p style={pStyle}>{props.description}</p>
			</a>);
}